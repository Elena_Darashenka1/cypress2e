import Faker from 'faker'
import BasePage from './cypress/pages/basePage'

export const mainPage = 'https://www.fitatu.com/'

const profileInfo = {
	password: Faker.internet.password(),
}

export default { profileInfo }
