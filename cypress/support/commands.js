const { MailSlurp } = require('mailslurp-client')
const apiKey =
	'd40730d460b8bf49e501c0fec1876b596f2fc255af072d3cc22ea58e816372f2'

Cypress.Commands.add('createInbox', () => {
	const mailslurp = new MailSlurp({ apiKey })
	return mailslurp.createInbox()
})

Cypress.Commands.add('waitForLatestEmail', inboxId => {
	const mailslurp = new MailSlurp({ apiKey })
	return mailslurp.waitForLatestEmail(inboxId)
})

Cypress.Commands.add('getEmailTextLines', (inboxId, decodeHtmlEntities) => {
	const mailslurp = new MailSlurp({ apiKey })
	const { body } = mailslurp.emailController.getEmailTextLines(
		inboxId,
		decodeHtmlEntities
	)
	return body
})

Cypress.Commands.add('isVisible', (locatorType, locator) => {
	if (locatorType == 'get') {
		cy.get(locator).should('be.visible')
	} else if (locatorType == 'contains') {
		cy.contains(locator).should('be.visible')
	}
})

Cypress.Commands.add('isNotExist', (locatorType, locator) => {
	if (locatorType == 'get') {
		cy.get(locator).should('not.exist')
	} else if (locatorType == 'contains') {
		cy.contains(locator).should('not.exist')
	}
})

Cypress.Commands.add('setResolution', size => {
	if (Cypress._.isArray(size)) {
		cy.viewport(size[0], size[1])
	} else {
		cy.viewport(size)
	}
})
