export default class BasePage {
	static clickOnButton(locatorType, locator) {
		if (locatorType == 'get') {
			cy.get(locator).click({ force: true })
		} else if (locatorType == 'contains') {
			cy.contains(locator).click({ force: true })
		}
	}

	static typeInField(locatorType, locator, text) {
		if (locatorType == 'get') {
			cy.get(locator).type(text)
		} else if (locatorType == 'contains') {
			cy.contains(locator).type(text)
		}
	}

	static checkCheckbox(locatorType, locator) {
		if (locatorType == 'get') {
			cy.get(locator).check()
		} else if (locatorType == 'contains') {
			cy.contains(locator).check()
		}
	}

	static goToURL(url) {
		cy.visit(url)
	}

	static checkElementIsVisible(locatorType, locator) {
		cy.isVisible(locatorType, locator)
	}

	static setLargeDesktopViewport() {
		cy.viewport(1980, 1080)
	}

	static checkElementHasValue(locator, value) {
		cy.get(locator).should('have.value', value)
	}

	static checkElementHasAttribute(locatorType, locator, value) {
		if (locatorType == 'get') {
			cy.get(locator).should('have.attr', value)
		} else if (locatorType == 'contains') {
			cy.contains(locator).should('have.attr', value)
		}
	}
}
