import { profilePage } from '../../../config'
import BasePage from '../../pages/basePage'
import { logInButton } from '../../locators/mainPageLocators'
import {
	createAccountButton,
	emailField,
	over16Checkbox,
	passwordField,
	processingDataCheckbox,
	termsAndConditionsCheckbox,
} from '../../locators/loginPageLocators'
import { logOut, myAccount } from '../../locators/plansPageLocators'
import data from '../../../config'

const { profileInfo } = data

describe('Sign in in the system successfully', () => {
	let inbox
	let emailAddress
	let inboxId

	it('should open main page and click to login button', () => {
		BasePage.goToURL('/en_GB')
		BasePage.clickOnButton('get', logInButton)
		BasePage.checkElementIsVisible(createAccountButton)
	})

	it('should click on "Create account" and check that registration page is visible', () => {
		BasePage.clickOnButton('contains', createAccountButton)
		BasePage.checkElementIsVisible('get', emailField)
	})

	it('should input registration info, click on Create button and check that user moved to plans page', () => {
		//create email first
		cy.createInbox().then(inbox => {
			assert.isDefined(inbox)
			emailAddress = inbox.emailAddress
			inboxId = inbox.id

			BasePage.typeInField('get', emailField, emailAddress)
			BasePage.typeInField('get', passwordField, profileInfo.password)
			BasePage.clickOnButton('get', termsAndConditionsCheckbox)
			BasePage.clickOnButton('get', over16Checkbox)
			BasePage.clickOnButton('get', processingDataCheckbox)
			BasePage.clickOnButton('contains', createAccountButton)
			BasePage.checkElementIsVisible('get', myAccount)
			BasePage.checkElementIsVisible('get', logOut)
		})
	})
	it('should receive confirmation email with confirmation link', () => {
		cy.wait(5000)
		cy.waitForLatestEmail(inboxId).then(email => {
			assert.isDefined(email)

			assert.strictEqual(
				/https:\/\/www.fitatu.com\/app\/activate-account\?userId=/.test(
					email.body
				),
				true
			)
		})
	})
})
